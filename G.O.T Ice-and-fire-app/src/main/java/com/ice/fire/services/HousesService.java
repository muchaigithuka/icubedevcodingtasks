package com.ice.fire.services;


import com.ice.fire.apiobjects.Characters;
import com.ice.fire.apiobjects.Houses;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/sys")
public class HousesService {

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping("/houses")
    public ModelAndView pullCharacters() {
        ModelAndView mv = new ModelAndView();
        List<Houses> hss = new ArrayList<Houses>();


        try {
            ResponseEntity<Houses[]> hsArray = restTemplate.getForEntity("https://www.anapioficeandfire.com/api/houses", Houses[].class);
            HttpClient client = HttpClientBuilder.create().build();
            Houses[] hsArrayBody = hsArray.getBody();
            for (Houses bk : hsArrayBody) {
                hss.add(bk);

                List<String> seats = bk.getSeats();
                      seats.forEach( (url) -> {
                          //                              HttpResponse response = client.execute( new HttpGet(url));
//                              int statusCode = response.getStatusLine().getStatusCode();

                              System.out.println(url);


                      });
            }

            mv.setViewName("houses/House");
            mv.addObject("hssArr", hss);

            return mv;
        } catch (Exception e) {
            e.printStackTrace();
        }

        mv.setViewName("houses/House");
        mv.addObject("hssArr", "Something Went Wrong Try Again Later");
        return mv;
    }


}


