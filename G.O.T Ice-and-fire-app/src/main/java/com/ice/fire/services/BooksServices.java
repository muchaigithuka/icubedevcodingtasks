package com.ice.fire.services;


import com.ice.fire.apiobjects.Books;
import com.ice.fire.apiobjects.Characters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/sys")
public class BooksServices {

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping("/books")
    public ModelAndView pullBooks(){
        ModelAndView mv = new ModelAndView();
        List<Books> books = new ArrayList<Books>();
        try {

            ResponseEntity<Books[]> bkArray = restTemplate.getForEntity("https://www.anapioficeandfire.com/api/books"  , Books[].class);
            Books[] bkArrayBody = bkArray.getBody();
            for(Books bk : bkArrayBody){
                 books.add(bk);
            }
            mv.setViewName("books/Books");
            mv.addObject("booksArr" , books);
            return mv;

           } catch (Exception e){
           e.printStackTrace();
          }
        mv.setViewName("books/Books");
        mv.addObject("payment" , "Something Went Wrong Try Again Later");
        return mv;
          }

}
