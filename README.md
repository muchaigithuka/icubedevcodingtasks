## iCUBE Coding Tasks Solutions by JOHN MUCHAI


## Repo Consists Of Four (4) Projects

  - Darts Application
  - Solution to Knapsack Limit Problem
  - GOT ( Game Of Thrones API Resources)
  - Rest API ( Jo's Solution that helps him track who he owes or who owes him)

## NB : All Applications are Java/Spring - Based Solutions

## What You Need

- Apache Maven 3.6.3 - Dependency Management
- JDK 1.8 or later - Platform Dependency 
- MysqlDb Version 8.0.22 - DataSource

## Now To the Applications

## Question One ( Darts App )

What I Build

- Given any point within the target , I evaluated the diameter within which a dart can land . 

- How ? The origin is (0 , 0) and target is (x , y) . Using mathematical formulae , I can evaluate Diameter , d .

- [See Implementation](https://gitlab.com/muchaigithuka/icubedevcodingtasks/-/blob/master/dart-app/src/main/java/com/darts/impl/Darts.java)

- There are maximum diameters (20 , 10 , 2) that a dart can land , given that the question provides radii (10 , 5  , 1)

- You can supply any point (x , y) , evaluate d && hence determine how many points the cartesian-point gives you .

![Screenshot](/img/darts-app-test.png)


## Question Two (Bob's Maximum Loot He Can Carry)
What I Build

- The question was how we can make Bob carry the maximum value from the items in the house. 

- I implemented a solution that helps Bob get a maximum value of 90.
 
![Screenshot](/img/knapsack-app-test.png)


[Link To Bob's Solution](https://gitlab.com/muchaigithuka/icubedevcodingtasks/-/tree/master/bob-max-achievable-value-and-weight)

## Question Three (Game of Thrones API resource querying , Simple Web Interface)
What I Build
 - API to pull characters resource and web functionality to open a character’s details in a new tab

![Screenshot](/img/pull-characters.png)



 - API to pull Books resource and web functionality to open a book-API field details in a new tab

![Screenshot](/img/pull-books.png)

- API to pull Houses resource and web functionality to open a house-API field details in a new tab

![Screenshot](/img/pull-houses.png)


I ran a few Tests : 

![Screenshot](/img/got-tests.png)


[Codebase for G.O.T Application](https://gitlab.com/muchaigithuka/icubedevcodingtasks/-/tree/master/G.O.T%20Ice-and-fire-app)



## Question Four (RESTful API that tracks Jo's borrowing habits and shows how much he owes.)

What I Build

Quick One , before we dive into API resource endpoints ;

[Set Up The Database](https://gitlab.com/muchaigithuka/icubedevcodingtasks/-/blob/master/Jo-borrowing-Tracker/sql.sql)

[Import Postman Collection](https://gitlab.com/muchaigithuka/icubedevcodingtasks/-/blob/master/Jo-borrowing-Tracker/Solution%20To%20Solves%20The%20Jo%20and%20and%20his%20room-mates%20lending%20problem.postman_collection.json)


- API to add user `/add`


![Screenshot](/img/add-user.png)

- API to find all users `/users`

![Screenshot](/img/list-users.png)

- API to POST `/iou`


The POST iou request can create the iou , update accounts for lender and borrower , and the provide  summary information to the specified lender in the IOU object sent .

As such ,  the lender in the POST IOU request object will receive information of who owes him and how much he is owed and the aggregated balance .

All in one POST request .

![Screenshot](/img/create-iou.png)


See database below 

![Screenshot](/img/sql.png)

I ran a few Tests:

![Screenshot](/img/jo-tests.png)


[Codebase for Jo's App ,Tracker for His room-mates Borrowing Habits](https://gitlab.com/muchaigithuka/icubedevcodingtasks/-/tree/master/Jo-borrowing-Tracker)
