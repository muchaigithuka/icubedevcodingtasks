package com.darts.impl;

public class Darts {
    int xOrigin = 0;
    int yOrigin = 0;


    public int score(int xCood , int yCood){

        //STEP ONE GET THE DIAMETER OF A CIRCLE GIVEN ANY TWO POINTS AND (0 , 0) AS THE REFERENCE

        double diameter = Math.sqrt(
                Math.pow((xOrigin - xCood) , 2) +
                Math.pow((yOrigin - yCood) , 2)
                                    );

        //EVALUATE FOR OUTER CIRCLE , ITS RADIUS IS 10 , SO DIAMETER IS 20
        if(diameter > 20){
            return 0;
        }else if(diameter <=20 && diameter > 10){

            return 1;
        }else if(diameter <=10 && diameter > 2){
            return 5;
        }else if(diameter <=2){
            return 10;
        }

        return 0;

    }
}
