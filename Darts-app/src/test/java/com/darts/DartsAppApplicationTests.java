package com.darts;

import com.darts.impl.Darts;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class DartsAppApplicationTests {

	@Test
	public void testDartWithinMiddleTarget() {
		Darts darts = new Darts();
		int score = darts.score(7, 2);
		assertEquals(5, score);

	}

	@Test
	public void testDartWithinOuterTarget() {
		Darts darts = new Darts();
		int score = darts.score(7, 11);
		assertEquals(1, score);

	}

	@Test
	public void testDartWithinInnerTarget() {
		Darts darts = new Darts();
		int score = darts.score(1, 1);
		assertEquals(10, score);

	}
}
