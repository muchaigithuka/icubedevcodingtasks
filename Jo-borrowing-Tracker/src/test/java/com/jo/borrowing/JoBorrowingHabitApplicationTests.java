package com.jo.borrowing;

import com.jo.borrowing.apis.UserApi;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class JoBorrowingHabitApplicationTests {

    @Autowired
	UserApi userApi;

//	@Autowired
//	RestTemplate restTemplate;


	@Test
	public void userApiService() throws Exception {
		assertThat(userApi).isNotNull();
	}

	@Test
	public void users() throws Exception {
		assertThat(userApi.findAllUsers()).isNotNull();
	}


}
