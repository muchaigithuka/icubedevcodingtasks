package com.jo.borrowing.apiobjects;

import java.util.Objects;

public class IOUObject {
    private String lender;
    private String borrower;
    private Double amount;

    public String getLender() {
        return lender;
    }

    public void setLender(String lender) {
        this.lender = lender;
    }

    public String getBorrower() {
        return borrower;
    }

    public void setBorrower(String borrower) {
        this.borrower = borrower;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "IOUObject{" +
                "lender='" + lender + '\'' +
                ", borrower='" + borrower + '\'' +
                ", amount=" + amount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IOUObject)) return false;
        IOUObject iouObject = (IOUObject) o;
        return Objects.equals(getLender(), iouObject.getLender()) && Objects.equals(getBorrower(), iouObject.getBorrower()) && Objects.equals(getAmount(), iouObject.getAmount());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLender(), getBorrower(), getAmount());
    }
}
