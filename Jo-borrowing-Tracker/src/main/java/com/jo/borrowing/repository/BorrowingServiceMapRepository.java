package com.jo.borrowing.repository;

import com.jo.borrowing.models.BorrowingServiceMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BorrowingServiceMapRepository extends JpaRepository<BorrowingServiceMap , Integer> {


    List<BorrowingServiceMap> findByName(@Param("lender") String lender);

    List<BorrowingServiceMap> findByLenderAndBorrower(@Param("lender") String lender , @Param("borrower") String borrower);

    List<BorrowingServiceMap> findByBorrowerAndLender(@Param("borrower") String borrower , @Param("lender") String lender);

    List<BorrowingServiceMap> calculateOwed(@Param("borrower") String lender , @Param("lender") String borrower);
}
