package com.jo.borrowing.repository;

import com.jo.borrowing.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {



    List<User> findAllExceptRequestName(@Param("name") String name);

    User findByRequestName(@Param("name") String name);
}
